# STP front-end written in yew.rs

## Setup

Add utility to build and serve WASM apps:

`cargo install trunk`

Add WASM to build targets:

`rustup target add wasm32-unknown-unknown`

Start the app:

`trunk serve`
