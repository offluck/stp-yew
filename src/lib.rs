pub mod app;
pub mod assets;
pub mod config;
pub mod router;
mod routes;
pub mod utils;

pub mod endpoints {
    pub const BACKEND_URL: &str = "http://localhost:8080";
    pub const BACKEND_LOGIN: &str = "http://localhost:8080/login";
    pub const BACKEND_USERS: &str = "http://localhost:8080/users";
}

pub const SALT: &str = "salty_string";
