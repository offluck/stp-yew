use crate::assets::images::LOGO_PATH;
use crate::router::{switch, Route};
use yew::prelude::*;
use yew_router::{BrowserRouter, Switch};

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <main>
            <div class="logo">
                <img class="logo" src={ LOGO_PATH } alt="Face" />
            </div>

            <BrowserRouter>
                <Switch<Route> render={switch} />
            </BrowserRouter>
        </main>
    }
}
