use std::cell::RefCell;
use stp_yew::app::App;
use stp_yew::config::Config;
use stp_yew::console_log;

thread_local! {
    static CONFIG: RefCell<Config> = RefCell::new(Config::default());
}

pub fn get_config() -> Config {
    CONFIG.with(|config| config.borrow().clone())
}

fn set_config(new_config: Config) {
    CONFIG.with(|config| {
        *config.borrow_mut() = new_config;
    });
}

fn main() {
    yew::Renderer::<App>::new().render();

    let config = Config::import().unwrap();
    set_config(config);

    console_log!("Config: {:?}", get_config());
}
