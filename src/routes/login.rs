use serde::{Deserialize, Serialize};
use yew::prelude::*;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct UserLogin {
    email: String,
    password: String,
}

impl UserLogin {
    fn new(email: String, password: String) -> Self {
        Self { email, password }
    }
}

async fn send_login_request(user: UserLogin) -> Result<(), Box<dyn std::error::Error>> {
    let user_json = serde_json::to_string(&user)?;
    let resp = reqwest::Client::builder()
        .build()?
        .post(crate::endpoints::BACKEND_LOGIN)
        .header("type", "application/json")
        .header("content-type", "application/json")
        .body(user_json)
        .send()
        .await?;

    if !resp.status().is_success() {
        return Err(format!("failed to post: {:?}", resp).into());
    }
    Ok(())
}

async fn send_login_callback(
    email: UseStateHandle<Option<String>>,
    password: UseStateHandle<Option<String>>,
) -> Callback<MouseEvent> {
    let email = match (*email).clone() {
        Some(val) => val,
        None => return crate::callback_msg!("please provide user's email"),
    };

    let password = match (*password).clone() {
        Some(val) => val,
        None => return crate::callback_msg!("please provide user's password"),
    };

    match send_login_request(UserLogin::new(email, password)).await {
        Ok(()) => crate::callback_msg!("success"),
        Err(err) => crate::callback_msg!("bad request", err),
    }
}

#[function_component(LoginForm)]
pub fn login() -> Html {
    let email = use_state(|| Option::<String>::None);
    let password = use_state(|| Option::<String>::None);
    let callback = use_state(|| Callback::from(|_| {}));

    let callback_callback = callback.clone();
    let email_callback = email.clone();
    let password_callback = password.clone();

    let submit = Callback::from(move |_| {
        let callback_callback = callback_callback.clone();
        let email_callback = email_callback.clone();
        let password_callback = password_callback.clone();

        wasm_bindgen_futures::spawn_local(async move {
            callback_callback.set(send_login_callback(email_callback, password_callback).await)
        });
    });

    html! {
        <>
            <div class="email">
                <p>{format!("Email*: {:?}", (*email).clone())}</p>
                <input type="text" placeholder="graydon@pobox.com" onchange={super::setter(email.clone(), false)} />
            </div>

            <div class="password">
                <p>{format!("Password*: {:?}", (*password).clone())}</p>
                <input type="password" placeholder="safe_password" onchange={super::setter(password.clone(), true)} />
            </div>

            <button onclick={submit}>{"Submit"}</button>
        </>
    }
}
