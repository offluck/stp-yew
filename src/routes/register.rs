use serde::{Deserialize, Serialize};
use yew::prelude::*;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct UserRegistration {
    name: String,
    email: String,
    password: String,
    biography: Option<String>,
    image_path: Option<String>,
    app_role_id: i32,
}

impl UserRegistration {
    fn new(name: String, email: String, password: String, biography: Option<String>) -> Self {
        Self {
            name,
            email,
            password,
            biography,
            image_path: None,
            app_role_id: 1,
        }
    }
}

async fn send_registration_request(
    user: UserRegistration,
) -> Result<(), Box<dyn std::error::Error>> {
    let user_json = serde_json::to_string(&user)?;
    crate::console_log!("{:?}", user_json);
    let resp = reqwest::Client::builder()
        .build()?
        .post(crate::endpoints::BACKEND_USERS)
        .header("type", "application/json")
        .header("content-type", "application/json")
        .body(user_json)
        .send()
        .await?;

    if !resp.status().is_success() {
        return Err(format!("failed to post: {:?}", resp).into());
    }
    Ok(())
}

async fn send_registration_callback(
    name: UseStateHandle<Option<String>>,
    email: UseStateHandle<Option<String>>,
    password: UseStateHandle<Option<String>>,
    biography: UseStateHandle<Option<String>>,
) {
    let name = match (*name).clone() {
        Some(val) => val,
        None => {
            crate::alert!("please provide user's name");
            return;
        }
    };

    let email = match (*email).clone() {
        Some(val) => val,
        None => {
            crate::alert!("please provide user's email");
            return;
        }
    };

    let password = match (*password).clone() {
        Some(val) => val,
        None => {
            crate::alert!("please provide user's password");
            return;
        }
    };

    let biography = (*biography).clone();

    match send_registration_request(UserRegistration::new(name, email, password, biography)).await {
        Ok(()) => {
            crate::alert!("success");
        },
        Err(err) => {
            crate::alert!("bad request: {:?}", err);
        }
    };
}

#[function_component(RegistrationForm)]
pub fn registration() -> Html {
    let name = use_state(|| Option::<String>::None);
    let email = use_state(|| Option::<String>::None);
    let password = use_state(|| Option::<String>::None);
    let biography = use_state(|| Option::<String>::None);

    let name_callback = name.clone();
    let email_callback = email.clone();
    let password_callback = password.clone();
    let biography_callback = biography.clone();

    let register = Callback::from(move |_| {
        let name_callback = name_callback.clone();
        let email_callback = email_callback.clone();
        let password_callback = password_callback.clone();
        let biography_callback = biography_callback.clone();

        wasm_bindgen_futures::spawn_local(async move {
            send_registration_callback(
                name_callback,
                email_callback,
                password_callback,
                biography_callback,
            )
            .await;
        });
    });

    html! {
        <>
            <div class="name">
                <p>{format!("Name*: {:?}", (*name).clone())}</p>
                <input type="text" placeholder="Graydon Hoare" onchange={super::setter(name.clone(), false)} />
            </div>

            <div class="email">
                <p>{format!("Email*: {:?}", (*email).clone())}</p>
                <input type="text" placeholder="graydon@pobox.com" onchange={super::setter(email.clone(), false)} />
            </div>

            <div class="password">
                <p>{format!("Password*: {:?}", (*password).clone())}</p>
                <input type="password" placeholder="safe_password" onchange={super::setter(password.clone(), true)} />
            </div>

            <div class="biography">
                <p>{format!("Biography: {:?}", (*biography).clone())}</p>
                <textarea name="biography" cols="40" rows="5" placeholder="Creator of safe(🔒) and blazingly fast(⚡️) Rust(🚀) programming language" onchange={super::setter(biography.clone(), false)}></textarea>
            </div>

            <button onclick={register}>{"Register"}</button>
        </>
    }
}
