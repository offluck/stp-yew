pub mod login;
pub mod register;

use web_sys::HtmlInputElement;
use yew::prelude::*;

pub(crate) fn setter(
    state: UseStateHandle<Option<String>>,
    needs_to_be_hashed: bool,
) -> Callback<Event> {
    Callback::from(move |e: Event| {
        let input: HtmlInputElement = e.target_unchecked_into();
        match input.value().is_empty() {
            true => state.set(None),
            false => match needs_to_be_hashed {
                true => state.set(Some(format!(
                    "{:x}",
                    md5::compute(input.value() + crate::SALT)
                ))),
                false => state.set(Some(input.value())),
            },
        }
        crate::console_log!("{:?}", *state);
    })
}
