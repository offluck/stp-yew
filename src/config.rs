#[derive(Debug, Default, Clone)]
pub struct Config {
    pub salt: String,
}

impl Config {
    pub fn import() -> anyhow::Result<Self> {
        Ok(Self {
            salt: crate::SALT.to_string(),
        })
    }
}
