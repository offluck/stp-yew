use crate::routes::login::LoginForm;
use crate::routes::register::RegistrationForm;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Debug, Clone, Copy, PartialEq, Routable)]
pub enum Route {
    #[at("/")]
    Main,
    #[at("/login")]
    Login,
    #[at("/registration")]
    Registration,
    #[not_found]
    #[at("/not-found")]
    NotFound,
}

pub fn switch(route: Route) -> Html {
    match route {
        Route::Main => html! {
            <>
                <h1> {"STP"} </h1>
            </>
        },
        Route::Login => html! {
            <>
                <h1> {"Login"} </h1>
                <LoginForm/>
            </>
        },
        Route::Registration => html! {
            <>
                <h1> {"Register"} </h1>
                <RegistrationForm/>
            </>
        },
        Route::NotFound => html! {
            <>
                <h1> {"Page not found!"} </h1>
            </>
        },
    }
}
