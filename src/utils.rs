#[macro_export]
macro_rules! console_log {
    ($($arg:tt)*) => {{
        yew_stdweb::services::ConsoleService::log(&format!($($arg)*));
    }};
}

#[macro_export]
macro_rules! alert {
    ($($arg:tt)*) => {{
        yew_stdweb::services::DialogService::alert(&format!($($arg)*));
    }};
}

#[macro_export]
macro_rules! callback_msg {
    ($msg:expr $(,)?) => {
        Callback::from(move |_| {
            crate::console_log!("{:?}", $msg);
            crate::alert!("{:?}", $msg);
        })
    };

    ($msg:expr, $($field:expr),* $(,)?) => {{
        let mut message = format!("{:?}: ", $msg);
        $(
            message = format!("{}{}, ", message, $field);
        )*
        message = message.trim_matches(&[',', ' '] as &[_]).to_string();

        Callback::from(move |_| {
            crate::console_log!("{:?}", message);
            crate::alert!("{:?}", message);
        })
    }};
}
